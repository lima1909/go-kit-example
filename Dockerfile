FROM ubuntu:18.10

COPY server server

EXPOSE 8080

ENTRYPOINT [ "./server" ]