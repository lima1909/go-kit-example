run:
	@go run main.go

add:
	curl -XPOST -d'{"id": 1, "name": "Mario"}' localhost:8080/add

rename:
	curl -XPOST -d'{"newName": "Mega", "user" : {"id": 1, "name": "Mario"}}' localhost:8080/rename

list:
	curl -XGET localhost:8080/list

# list with filter
list2:
	curl -XGET localhost:8080/list?filter=InitUser


prepare:
	@echo "Go-Version: " $(shell go version)
	go get -t ./...
	go get -u github.com/golangci/golangci-lint/cmd/golangci-lint

test:
	go tool vet .
	go test -race -count=1  ./...
	golangci-lint run  --disable=megacheck ./...

docker:
	go build -o server
	docker build -t lima1909/hub:go-kit-example .
	rm server