package business

import (
	"fmt"
)

// User datastruct
type User struct {
	ID   int    `jason:"id"`
	Name string `jason:"name"`
}

func (u User) String() string {
	return fmt.Sprintf("ID: %v Name: %v", u.ID, u.Name)
}

// Registry for users
type Registry interface {
	AddUser(u User) (User, error)
	Rename(u User, newname string) (User, error)
	List(filter string) ([]User, error)
}

// ARegistry example impl from Registry
type ARegistry struct {
	users []User
}

// NewRegistry constructor
func NewRegistry() *ARegistry {
	users := make([]User, 0)
	users = append(users, User{ID: 0, Name: "InitUser"})

	return &ARegistry{
		users: users,
	}
}

// AddUser impl from interface Registry
func (r *ARegistry) AddUser(u User) (User, error) {
	r.users = append(r.users, u)
	return u, nil
}

// Rename impl from interface Registry
func (r *ARegistry) Rename(user User, newname string) (User, error) {
	for _, u := range r.users {
		if u.Name == user.Name {
			u.Name = newname
			return u, nil
		}
	}
	return User{}, fmt.Errorf("No User found with Name: %s", newname)
}

// List impl from interface Registry
func (r *ARegistry) List(filter string) ([]User, error) {
	if filter == "" {
		return r.users, nil
	}

	newUsers := make([]User, 0)
	for _, u := range r.users {
		if u.Name == filter {
			newUsers = append(newUsers, u)
		}
	}
	return newUsers, nil
}
