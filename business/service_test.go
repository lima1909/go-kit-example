package business

import "testing"

func TestAdd(t *testing.T) {
	r := NewRegistry()
	u, err := r.AddUser(User{ID: 1, Name: "Foo"})
	if err != nil {
		t.Errorf("no err expected; %v", err)
	}
	if u.Name == "" {
		t.Errorf("expect Foo and nor empts Name")
	}

	l, _ := r.List("")
	if 2 != len(l) {
		t.Errorf("expect 2, but is %v", len(l))
	}
}
