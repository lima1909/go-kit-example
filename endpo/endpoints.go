package endpo

import (
	"context"
	"fmt"

	"github.com/go-kit/kit/endpoint"
	"gitlab.com/lima1909/go-kit-example/business"
)

// AddUser Endpoint for AddUser
func AddUser(svc business.Registry) endpoint.Endpoint {
	return func(c context.Context, r interface{}) (interface{}, error) {
		u := r.(business.User)
		return svc.AddUser(u)
	}
}

// RenameReq req for rename
type RenameReq struct {
	User    business.User `jason:"user"`
	NewName string        `json:"newName"`
}

func (r RenameReq) String() string {
	return fmt.Sprintf("User: %v (NewName: %s)", r.User, r.NewName)
}

// Rename Endpoint for Rename
func Rename(svc business.Registry) endpoint.Endpoint {
	return func(c context.Context, r interface{}) (interface{}, error) {
		rr := r.(RenameReq)
		return svc.Rename(rr.User, rr.NewName)
	}
}

// List Endpoint for List
func List(svc business.Registry) endpoint.Endpoint {
	return func(c context.Context, r interface{}) (interface{}, error) {
		filter := r.(string)
		return svc.List(filter)
	}
}
