package main

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"os"

	"gitlab.com/lima1909/go-kit-example/middleware"

	logk "github.com/go-kit/kit/log"
	httptransport "github.com/go-kit/kit/transport/http"
	"gitlab.com/lima1909/go-kit-example/business"
	"gitlab.com/lima1909/go-kit-example/endpo"
)

func main() {
	logger := logk.NewLogfmtLogger(os.Stderr)
	svc := business.NewRegistry()

	addHandler := httptransport.NewServer(
		middleware.LoggingMiddleware(logk.With(logger, "add", "log"))(
			endpo.AddUser(svc),
		),
		decodeUser,
		encodeResponse,
	)

	renameHandler := httptransport.NewServer(
		middleware.LoggingMiddleware(logk.With(logger, "rename", "log"))(
			endpo.Rename(svc),
		),
		decodeRenameReq,
		encodeResponse,
	)

	listHandler := httptransport.NewServer(
		middleware.StoppingMiddleware(logk.With(logger, "list", "stop"))(
			middleware.LoggingMiddleware(logk.With(logger, "list", "log"))(
				endpo.List(svc),
			),
		),
		decodeList,
		encodeResponse,
	)

	http.Handle("/add", addHandler)
	http.Handle("/rename", renameHandler)
	http.Handle("/list", listHandler)

	log.Println("Run server on localhost:8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func decodeUser(_ context.Context, r *http.Request) (interface{}, error) {
	var user business.User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		return nil, err
	}
	return user, nil
}

func decodeRenameReq(_ context.Context, r *http.Request) (interface{}, error) {
	var renameReq endpo.RenameReq
	if err := json.NewDecoder(r.Body).Decode(&renameReq); err != nil {
		return nil, err
	}
	return renameReq, nil
}

func decodeList(_ context.Context, r *http.Request) (interface{}, error) {
	val := r.URL.Query()
	filter := val.Get("filter")
	return filter, nil
}

func encodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}
