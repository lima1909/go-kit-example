package middleware

import (
	"context"
	"time"

	"github.com/go-kit/kit/endpoint"
	logk "github.com/go-kit/kit/log"
)

// LoggingMiddleware ...
func LoggingMiddleware(logger logk.Logger) endpoint.Middleware {
	return func(next endpoint.Endpoint) endpoint.Endpoint {
		return func(ctx context.Context, request interface{}) (res interface{}, err error) {
			_ = logger.Log("msg", "before", "req", request)
			res, err = next(ctx, request)
			_ = logger.Log("msg", "after ", "res", res)
			return res, err
		}
	}
}

// StoppingMiddleware ...
func StoppingMiddleware(logger logk.Logger) endpoint.Middleware {
	return func(next endpoint.Endpoint) endpoint.Endpoint {
		return func(ctx context.Context, request interface{}) (res interface{}, err error) {
			t := time.Now()
			_ = logger.Log("msg", "before", "start", t)
			res, err = next(ctx, request)
			time.Sleep(time.Millisecond * 200)
			_ = logger.Log("msg", "after ", "duration", time.Since(t))
			return res, err
		}
	}
}
